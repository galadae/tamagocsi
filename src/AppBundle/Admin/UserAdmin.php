<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('username', null, array('label' => 'admin.label.user.username'))
            ->add('email', null, array('label' => 'admin.label.user.email'))
            ->add('actualLevel', null, array('label' => 'admin.label.user.actual_level'))
            ->add('actualXp', null, array('label' => 'admin.label.user.actual_xp'))
            ->add('requiredXpToLvlUp', null, array('label' => 'admin.label.user.required_xp_to_lvl_up'))
            ->add('credit', null, array('label' => 'admin.label.user.credit'))
            ->add('gym', null, array('label' => 'admin.label.user.structure.gym'))
            ->add('garden', null, array('label' => 'admin.label.user.structure.garden'))
            ->add('workshop', null, array('label' => 'admin.label.user.structure.workshop'));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $roles = $container->getParameter('security.role_hierarchy.roles');
        $rolesChoices = self::roles($roles);

        $formMapper
            ->add('username', null, array('label' => 'admin.label.user.username'))
            ->add('email', null, array('label' => 'admin.label.user.email'))
            ->add('plainPassword', 'text', array('required' => false, 'label' => 'admin.label.user.plain_password'))
            ->add('roles', 'choice', array(
                    'choices' => $rolesChoices,
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'admin.label.user.roles'
                )
            );

        if (!$this->getSubject()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->add('enabled', null, array('required' => false, 'label' => 'admin.label.user.enabled'));
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('id', null, array('label' => 'admin.label.user.id'))
            ->add('username', null, array('label' => 'admin.label.user.username'))
            ->add('email', null, array('label' => 'admin.label.user.email'))
            ->add('actualLevel', null, array('label' => 'admin.label.user.actual_level'))
            ->add('credit', null, array('label' => 'admin.label.user.credit'));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username', null, array('label' => 'admin.label.user.username'))
            ->add('email', null, array('label' => 'admin.label.user.email'))
            ->add('enabled', null, array('editable' => true, 'label' => 'admin.label.user.enabled'))
            ->add('actualLevel', null, array('label' => 'admin.label.user.actual_level'))
            ->add('actualXp', null, array('label' => 'admin.label.user.actual_xp'))
            ->add('requiredXpToLvlUp', null, array('label' => 'admin.label.user.required_xp_to_lvl_up'))
            ->add('credit', null, array('label' => 'admin.label.user.credit'))
            ->add('gym', null, array('label' => 'admin.label.user.structure.gym'))
            ->add('garden', null, array('label' => 'admin.label.user.structure.garden'))
            ->add('workshop', null, array('label' => 'admin.label.user.structure.workshop'));

        if ($this->isGranted('ROLE_USER_MANAGE')) {
            $listMapper
                ->add(
                    '_action',
                    null,
                    [
                        'actions' => [
                            'show' => [],
                            'edit' => [],
                            'delete' => [],
                            'add_xp' => [
                                'template' => 'AppBundle:admin/custom_actions:add_xp.html.twig'
                            ],
                            'level_up_structure' => [
                                'template' => 'AppBundle:admin/custom_actions:level_up_structure.html.twig'
                            ]
                        ],
                        'label' => 'admin.label.user.actions'
                    ]);
        } else {
            $listMapper
                ->add(
                    '_action',
                    null,
                    [
                        'actions' => [
                            'show' => [],
                        ],
                        'label' => 'admin.label.user.actions'
                    ]);
        }

        /*if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            $listMapper
                ->add('impersonating', 'string', array('template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig', 'label' => 'admin.label.user.impersonating'));
        }*/
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('add_xp', $this->getRouterIdParameter().'/add_xp');
        $collection->add('level_up_structure', $this->getRouterIdParameter().'/level_up_structure');
    }

    protected static function roles($rolesHierarchy)
    {
        $roleClasses = [];
        foreach ($rolesHierarchy as $class => $roles) {
            $roleClasses[$class] = $class;
        }
        return $roleClasses;
    }

}
