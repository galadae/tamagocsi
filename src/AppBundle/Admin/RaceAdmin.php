<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use AppBundle\Handler\ResourceHandler;

class RaceAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name', null, array('label' => 'admin.label.race.name'))
            ->add('unlockLevel', null, array('label' => 'admin.label.race.unlock_level'))
            ->add('hunger', null, array('label' => 'admin.label.race.hunger'))
            ->add('reservation', null, array('label' => 'admin.label.race.reservation'))
            ->add('comment', null, array('label' => 'admin.label.race.comment'))
            ->add('createdAt', null, array('label' => 'admin.label.race.created_at'));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $resourceTypeHandler = new ResourceHandler();
        $formMapper
            ->add('name', null, array('label' => 'admin.label.race.name'))
            ->add('unlockLevel', null, array('label' => 'admin.label.race.unlock_level'))
            ->add('hunger', null, array('label' => 'admin.label.race.hunger'))
            ->add('reservation', null, array('label' => 'admin.label.race.reservation'))
            ->add('comment', null, array('label' => 'admin.label.race.comment'));
            /*->add('resourceTypes', 'choice', array(
                    'choices' => $resourceTypeHandler->getAvailableResourceTypes(),
                    'expanded' => true,
                )
            );*/
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('name', null, array('label' => 'admin.label.race.name'))
            ->add('unlockLevel', null, array('label' => 'admin.label.race.unlock_level'))
            ->add('hunger', null, array('label' => 'admin.label.race.hunger'));
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $resourceTypeHandler = new ResourceHandler();
        $listMapper
            ->add('name', null, array('label' => 'admin.label.race.name'))
            ->add('unlockLevel', null, array('label' => 'admin.label.race.unlock_level'))
            ->add('hunger', null, array('label' => 'admin.label.race.hunger'))
            ->add('reservation', null, array('label' => 'admin.label.race.reservation'))
            ->add('comment', null, array('label' => 'admin.label.race.comment'))
            ->add('createdAt', null, array('label' => 'admin.label.race.created_at'))
            ->add(
                '_action',
                null,
                [
                    'actions' => [
                        'show' => [],
                        'edit' => [],
                        'delete' => []
                    ],
                    'label' => 'admin.label.race.actions'
                ]);

    }
}
