<?php
/**
 * Created by PhpStorm.
 * User: galadae
 * Date: 2016.12.04.
 * Time: 11:01
 */

namespace AppBundle\Handler;


use AppBundle\Entity\Buildings\Structure;
use AppBundle\Entity\User;

class ResourceHandler
{
    protected $initArray = [
        #essence
        'Earth essence' => 0,
        'Water essence' => 0,
        'Fire essence' => 0,
        'Air essence' => 0,
        'Light essence' => 0,
        'Dark essence' => 0,
        #build
        'Metal' => 0,
        'Wood' => 0,
        'Stone' => 0,
        #craft
        'Fur' => 0,
        'Scale' => 0,
        'Silk' => 0,
        'Feather' => 0,
        #'Ink',
        ##rare
        'Gold' => 0,
        'Holy water' => 0,
        'Dark cristal' => 0,
        'Light cristal' => 0,
    ];

    /**
     * @param string $resourceType
     * @return bool
     */
    public function isCorrectResourceType($resourceType)
    {
        return in_array($resourceType, array_keys($this->initArray));
    }

    public function getAvailableResourceTypes()
    {
        return [
            #essence
            'Earth essence' => 'Earth essence',
            'Water essence' => 'Water essence',
            'Fire essence' => 'Fire essence',
            'Air essence' => 'Air essence',
            'Light essence' => 'Light essence',
            'Dark essence' => 'Dark essence',
            #build
            'Metal' => 'Metal',
            'Wood' => 'Wood',
            'Stone' => 'Stone',
            #craft
            'Fur' => 'Fur',
            'Scale' => 'Scale',
            'Silk' => 'Silk',
            'Feather' => 'Feather',
            #'Ink',
            ##rare
            'Gold' => 'Gold',
            'Holy water' => 'Holy water',
            'Dark cristal' => 'Dark cristal',
            'Light cristal' => 'Light cristal',
        ];
    }

    /**
     * @return array
     */
    public function getResourceInitStatus()
    {
        $initStatus = [];
        foreach ($this->initArray as $type => $quantity) {
            $initStatus[$type] = $quantity;
        }
        return $initStatus;
    }

    /**
     * @param User $user
     * @param array $resources
     * @return bool
     */
    public function addResourcesToUser($user, $resources)
    {
        if (!is_array($resources)) {
            return false;
        }
        $actualResources = $user->getResources();

        foreach ($resources as $resource => $quantity) {
            $actualResources[$resource] += $quantity;
        }

        $user->setResources($actualResources);
        return true;
    }

    /**
     * @param User $user
     * @param array $resources
     * @return bool
     */
    public function removeResourcesFromUser($user, $resources)
    {
        if (!is_array($resources) || !self::userHasEnoughResurces($user, $resources)) {
            return false;
        }
        $actualResources = $user->getResources();

        foreach ($resources as $resource => $quantity) {
            $actualResources[$resource] -= $quantity;
        }

        $user->setResources($actualResources);
        return true;
    }

    /**
     * @param User $user
     * @param array $resources
     * @return bool
     */
    public function userHasEnoughResurces($user, $resources)
    {
        if (!is_array($resources)) {
            return false;
        }
        $availableResources = $user->getResources();
        foreach ($resources as $resource => $quantity) {
            if ($availableResources[$resource] < $quantity) {
                return false;
            }
        }
        return true;
    }

    /**
     * todo szint alapján számol és a fajon csak az arány van meghatározva
     *
     */
    public function productionCalculate()
    {

    }
}