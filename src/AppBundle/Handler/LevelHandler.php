<?php

namespace AppBundle\Handler;

use AppBundle\Entity\Animal;
use AppBundle\Entity\Buildings\Structure;
use AppBundle\Entity\User;

class LevelHandler
{
    /**
     * @param integer $level
     * @return int
     */
    protected function calculateUserNextLevelXp($level)
    {
        $xp = 0;
        for ($i = 1, $a = 1.005; $i <= $level; $i++) {
            $xp += pow(10, $a += ($i + 20) / 1000);
        }
        return (int)$xp;
    }

    /**
     * @param User $user
     */
    protected function userLevelUp($user)
    {
        $user->setActualLevel($user->getActualLevel() + 1);
        $user->setRequiredXpToLvlUp(self::calculateUserNextLevelXp($user->getActualLevel()));
    }

    /**
     * @param User $user
     * @return boolean
     */
    protected function needUserLevelUp($user)
    {
        return $user->getActualXp() >= $user->getRequiredXpToLvlUp();
    }

    /**
     * @param User $user
     * @param integer $xp
     */
    public function addXpToUser($user, $xp)
    {
        $user->setActualXp($user->getActualXp() + $xp);
        while (self::needUserLevelUp($user)) {
            self::userLevelUp($user);
        }
    }

    /**
     * @param Structure | Animal $item
     */
    public function initNonUser($item)
    {
        $item->setActualLevel(1);
        $item->setRequiredResourceToNextLevel($item->getBaseEssenceRequire());
    }

    /**
     * @param Structure $item
     */
    public function upgrade($item, $essenceType, $level = 1)
    {
        $resourceHandler = new ResourceHandler();
        if ($resourceHandler->removeResourcesFromUser($item->getOwner(), [$essenceType => $item->getRequiredResourceToNextLevel()])) {
            $item->setActualLevel($item->getActualLevel() + $level);
            $item->setRequiredResourceToNextLevel(self::getUpgradeRequiredResource($item));
        }
    }

    /**
     * @param Structure $item
     */
    public function upgradeWithoutResource($item, $level = 1)
    {
        $item->setActualLevel($item->getActualLevel() + $level);
        $item->setRequiredResourceToNextLevel(self::getUpgradeRequiredResource($item));
    }

    /**
     * @param Structure $item
     * @return int
     */
    public function getUpgradeRequiredResource($item)
    {
        return $item->getBaseEssenceRequire() * (pow(2, $item->getActualLevel() - 1));
    }
}