<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Race;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Handler\ResourceHandler;

class RaceData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function getOrder()
    {
        return 2;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $races = Yaml::parse(file_get_contents($this->container->get('kernel')->getRootDir() . '/../src/AppBundle/DataFixtures/Data/Races.yml'));
        $resourceTypeHandler = new ResourceHandler();

        foreach ($races as $race) {
            $entity = new Race();
            if (!empty($race['name'])) {
                $entity->setName($race['name']);
            }
            if (!empty($race['unlockLevel'])) {
                $entity->setUnlockLevel($race['unlockLevel']);
            }
            if (!empty($race['hunger'])) {
                $entity->setHunger($race['hunger']);
            }
            if (!empty($race['reservation'])) {
                $entity->setReservation($race['reservation']);
            }
            if (!empty($race['comment'])) {
                $entity->setComment($race['comment']);
            }
            if (!empty($race['resources'])) {
                foreach ($race['resources'] as $resource) {
                    if ($resourceTypeHandler->isCorrectResourceType($resource['name'])) {
                        $entity->addResource($resource);
                    }
                }
            }
            $manager->persist($entity);
        }
        $manager->flush();
    }
}