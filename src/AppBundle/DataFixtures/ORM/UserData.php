<?php
/**
 * Created by PhpStorm.
 * User: galadae
 * Date: 2016.12.03.
 * Time: 20:17
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class UserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function getOrder()
    {
        return 1;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $users = Yaml::parse(file_get_contents($this->container->get('kernel')->getRootDir().'/../src/AppBundle/DataFixtures/Data/Users.yml'));

        foreach ($users as $user) {
            $entity = new User();
            $entity->setUsername($user['name']);
            $entity->setPlainPassword($user['password']);
            $entity->setEmail($user['email']);
            $entity->addRole($user['role']);
            $entity->setEnabled(true);

            $manager->persist($entity);
        }

        $manager->flush();
    }


}