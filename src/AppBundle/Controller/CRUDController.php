<?php
/**
 * Created by PhpStorm.
 * User: galadae
 * Date: 2016.12.11.
 * Time: 11:08
 */

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use AppBundle\Handler\LevelHandler;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;

class CRUDController extends Controller
{
    public function addXpAction(Request $request)
    {

        /**
         * @var User $user
         */
        $user = $this->admin->getSubject();
        $levelHandler = new LevelHandler();
        $levelHandler->addXpToUser($user, $request->get('xp'));
        $manager = $this->getDoctrine()->getEntityManager();
        $manager->persist($user);
        $manager->flush();
        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function levelUpStructureAction(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $this->admin->getSubject();
        $levelHandler = new LevelHandler();
        $manager = $this->getDoctrine()->getEntityManager();

        switch ($request->get('type')) {
            case 'gym':
            {
                $structure = $user->getGym();
                break;
            }
            case 'garden':
            {
                $structure = $user->getGarden();
                break;
            }
            case 'workshop':
            {
                $structure = $user->getWorkshop();
                break;
            }
            default:
            {
                return false;
            }
        }
        $levelHandler->levelUpStructureWithoutResource($structure, $request->get('level'));

        $manager->persist($structure);
        $manager->flush();

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}