<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Buildings\Garden;
use AppBundle\Entity\Buildings\Gym;
use AppBundle\Entity\Buildings\Workshop;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Handler\LevelHandler;
use AppBundle\Handler\ResourceHandler;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $credit;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $actualLevel;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $actualXp;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $requiredXpToLvlUp;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $resources;

    /**
     * @var Gym
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Buildings\Gym", cascade={"persist"})
     */
    protected $gym;

    /**
     * @var Garden
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Buildings\Garden", cascade={"persist"})
     */
    protected $garden;

    /**
     * @var Workshop
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Buildings\Workshop", cascade={"persist"})
     */
    protected $workshop;

    public function __construct()
    {
        $xpHandler = new LevelHandler();
        $resourceHandler = new ResourceHandler();
        parent::__construct();
        self::setCredit(0);
        self::setActualLevel(0);
        self::setActualXp(0);
        self::setRequiredXpToLvlUp(0);
        $xpHandler->addXpToUser($this, 1);
        self::setResources($resourceHandler->getResourceInitStatus());
        self::setGym(new Gym());
        self::getGym()->setOwner($this);
        self::setGarden(new Garden());
        self::getGarden()->setOwner($this);
        self::setWorkshop(new Workshop());
        self::getWorkshop()->setOwner($this);
    }

    /**
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param int $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    }

    /**
     * @return int
     */
    public function getActualLevel()
    {
        return $this->actualLevel;
    }

    /**
     * @param int $actualLevel
     */
    public function setActualLevel($actualLevel)
    {
        $this->actualLevel = $actualLevel;
    }

    /**
     * @return int
     */
    public function getActualXp()
    {
        return $this->actualXp;
    }

    /**
     * @param int $actualXp
     */
    public function setActualXp($actualXp)
    {
        $this->actualXp = $actualXp;
    }

    /**
     * @return int
     */
    public function getRequiredXpToLvlUp()
    {
        return $this->requiredXpToLvlUp;
    }

    /**
     * @param int $requiredXpToLvlUp
     */
    public function setRequiredXpToLvlUp($requiredXpToLvlUp)
    {
        $this->requiredXpToLvlUp = $requiredXpToLvlUp;
    }

    /**
     * @return array
     */
    public function getResources()
    {
        return unserialize($this->resources);
    }

    /**
     * @param array $resources
     */
    public function setResources($resources)
    {
        $this->resources = serialize($resources);
    }

    /**
     * @return Gym
     */
    public function getGym()
    {
        return $this->gym;
    }

    /**
     * @param Gym $gym
     */
    public function setGym($gym)
    {
        $this->gym = $gym;
    }

    /**
     * @return Garden
     */
    public function getGarden()
    {
        return $this->garden;
    }

    /**
     * @param Garden $garden
     */
    public function setGarden($garden)
    {
        $this->garden = $garden;
    }

    /**
     * @return Workshop
     */
    public function getWorkshop()
    {
        return $this->workshop;
    }

    /**
     * @param Workshop $workshop
     */
    public function setWorkshop($workshop)
    {
        $this->workshop = $workshop;
    }

}