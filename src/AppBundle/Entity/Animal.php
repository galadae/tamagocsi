<?php
/**
 * Created by PhpStorm.
 * User: galadae
 * Date: 2016.12.16.
 * Time: 18:15
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Buildings\Garden;
use AppBundle\Handler\LevelHandler;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="animal")
 */
class Animal
{
    const MAXIMUM_LEVEL = 20;

    const BASE_ESSENCE_REQUIRE = 10;

    const BASE_ESSENCE_PRODUCTION = 5;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $actualLevel;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $requiredResourceToNextLevel;

    /**
     * @var Garden
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Buildings\Garden", inversedBy="animals")
     */
    protected $home;

    /**
     * @var Race
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Race")
     */
    protected $race;

    //todo
    protected $hp;

    public function __construct()
    {
        $levelHandler = new LevelHandler();
        $levelHandler->initNonUser($this);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getActualLevel()
    {
        return $this->actualLevel;
    }

    /**
     * @param int $actualLevel
     */
    public function setActualLevel($actualLevel)
    {
        $this->actualLevel = $actualLevel;
    }

    /**
     * @return int
     */
    public function getRequiredResourceToNextLevel()
    {
        return $this->requiredResourceToNextLevel;
    }

    /**
     * @param int $requiredResourceToNextLevel
     */
    public function setRequiredResourceToNextLevel($requiredResourceToNextLevel)
    {
        $this->requiredResourceToNextLevel = $requiredResourceToNextLevel;
    }

    /**
     * @return Garden
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * @param Garden $home
     */
    public function setHome($home)
    {
        $this->home = $home;
    }

    /**
     * @return Race
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @param Race $race
     */
    public function setRace($race)
    {
        $this->race = $race;
    }

    /**
     * @return int
     */
    public function getMaximumLevel()
    {
        return self::MAXIMUM_LEVEL;
    }

    /**
     * @return int
     */
    public function getBaseEssenceRequire()
    {
        return self::BASE_ESSENCE_REQUIRE;
    }

    /**
     * @return int
     */
    public function getBaseEssenceProduction()
    {
        return self::BASE_ESSENCE_PRODUCTION;
    }
}