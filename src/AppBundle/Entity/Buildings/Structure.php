<?php

namespace AppBundle\Entity\Buildings;

use AppBundle\Handler\LevelHandler;
use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

abstract class Structure
{
    const MAXIMUM_LEVEL = 20;

    const BASE_ESSENCE_REQUIRE = 10;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $actualLevel;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $requiredResourceToNextLevel;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $owner;

    public function __construct()
    {
        $levelHandler = new LevelHandler();
        $levelHandler->initNonUser($this);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getActualLevel()
    {
        return $this->actualLevel;
    }

    /**
     * @param int $actualLevel
     */
    public function setActualLevel($actualLevel)
    {
        $this->actualLevel = $actualLevel;
    }

    /**
     * @return int
     */
    public function getRequiredResourceToNextLevel()
    {
        return $this->requiredResourceToNextLevel;
    }

    /**
     * @param int $requiredResourceToNextLevel
     */
    public function setRequiredResourceToNextLevel($requiredResourceToNextLevel)
    {
        $this->requiredResourceToNextLevel = $requiredResourceToNextLevel;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    function __toString()
    {
        return ($this->getName() . ' | ' . $this->getActualLevel() . ' | ' . $this->getRequiredResourceToNextLevel());
    }

    /**
     * @return int
     */
    public function getMaximumLevel()
    {
        return self::MAXIMUM_LEVEL;
    }

    /**
     * @return int
     */
    public function getBaseEssenceRequire()
    {
        return self::BASE_ESSENCE_REQUIRE;
    }
}