<?php
/**
 * Created by PhpStorm.
 * User: galadae
 * Date: 2016.12.10.
 * Time: 16:35
 */

namespace AppBundle\Entity\Buildings;

use AppBundle\Entity\Animal;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="garden")
 */
class Garden extends Structure
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Animal", mappedBy="home")
     */
    protected $animals;

    public function __construct()
    {
        parent::__construct();
        self::setName('Garden');
        self::setAnimals(new ArrayCollection());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnimals()
    {
        return $this->animals;
    }

    /**
     * @param ArrayCollection $animals
     */
    public function setAnimals($animals)
    {
        $this->animals = $animals;
    }

    /**
     * @param Animal $animal
     */
    public function addAnimal($animal)
    {
        $this->animals->add($animal);
    }

    /**
     * @param Animal $animal
     */
    public function removeAnimal($animal)
    {
        $this->animals->remove($animal);
    }
}