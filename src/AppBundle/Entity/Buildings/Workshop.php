<?php
/**
 * Created by PhpStorm.
 * User: galadae
 * Date: 2016.12.10.
 * Time: 16:36
 */

namespace AppBundle\Entity\Buildings;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="workshop")
 */
class Workshop extends Structure
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        parent::__construct();
        self::setName('Workshop');
    }
}