<?php

namespace AppBundle\Entity;

use AppBundle\Handler\ResourceHandler;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="race")
 */
class Race
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $unlockLevel;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $hunger;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $reservation;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $resource;

    public function __construct()
    {
        self::setCreatedAt(new \DateTime());

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getUnlockLevel()
    {
        return $this->unlockLevel;
    }

    /**
     * @param int $unlockLevel
     */
    public function setUnlockLevel($unlockLevel)
    {
        $this->unlockLevel = $unlockLevel;
    }

    /**
     * @return int
     */
    public function getHunger()
    {
        return $this->hunger;
    }

    /**
     * @param int $hunger
     */
    public function setHunger($hunger)
    {
        $this->hunger = $hunger;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * @param int $reservation
     */
    public function setReservation($reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return array
     */
    public function getResource()
    {
        return unserialize($this->resource);
    }

    /**
     * @param array $resource
     */
    public function setResource($resource)
    {
        $this->resource = serialize($resource);
    }

    /**
     * @param array $resource
     */
    public function addResource($resource)
    {
        $arrayForm = unserialize($this->resource);
        $resourceTypeHandler = new ResourceHandler();
        if (
            $resourceTypeHandler->isCorrectResourceType($resource['name']) &&
            !in_array(
                $resource['name'],
                is_array($this->resource)?$this->resource:[]
            )
        ) {
            $arrayForm[$resource['name']] = $resource;
        }
        $this->resource = serialize($arrayForm);
    }

    /**
     * @param string $resource
     */
    public function removeResource($resource)
    {
        unset($this->resource[$resource]);
    }

    function __toString()
    {
        return (string)self::getName();
    }
}